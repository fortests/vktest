CREATE TABLE Player
(
    id        INT AUTO_INCREMENT PRIMARY KEY,
    vkId      INT                                   NOT NULL,
    firstName VARCHAR(255)                          NULL,
    lastName  VARCHAR(255)                          NOT NULL,
    createdAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP() NOT NULL,
    updatedAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP() NOT NULL ON UPDATE CURRENT_TIMESTAMP()
);

CREATE TABLE VkNotifier
(
    id          INT AUTO_INCREMENT PRIMARY KEY,
    message     VARCHAR(255)                          NOT NULL,
    batchNumber INT                                   NOT NULL,
    status      VARCHAR(255)                          NOT NULL,
    createdAt   TIMESTAMP DEFAULT CURRENT_TIMESTAMP() NOT NULL,
    updatedAt   TIMESTAMP DEFAULT CURRENT_TIMESTAMP() NOT NULL ON UPDATE CURRENT_TIMESTAMP()
);