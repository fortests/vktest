module.exports = {
    vkNotifier: {
        file: {
            level           : 'info',
            filename        : `./logs/vkNotifier.log`,
            handleExceptions: true,
            json            : true,
            maxsize         : 5242880, // 5MB
            maxFiles        : 5,
            colorize        : false,
        },
    },
};