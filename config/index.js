const db         = require('./db');
const vk         = require('./vk');
const vkNotifier = require('./vkNotifier');
const log        = require('./log');

module.exports = {
    db        : db,
    vk        : vk,
    vkNotifier: vkNotifier,
    log       : log,
};