module.exports = {
    maxCallApiCount: 3,
    limit          : 100,
    defaultTimeout : 1000,
    maxTimeoutUps  : 3,
};