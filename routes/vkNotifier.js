const express              = require('express');
const router               = express.Router();
const VkNotifierController = require('../controllers/VkNotifierController');

router.get('/send', function (req, res, next) {
    VkNotifierController.send(req, res, next);
});
router.get('/stop', function (req, res, next) {
    VkNotifierController.stop(req, res, next);
});
router.get('/resume', function (req, res, next) {
    VkNotifierController.resume(req, res, next);
});
router.get('/get-info', function (req, res, next) {
    VkNotifierController.getInfo(req, res, next);
});

module.exports = router;
