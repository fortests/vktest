const createError     = require('http-errors');
const express         = require('express');
const helmet          = require('helmet');
const morgan          = require('morgan');
const router          = require('./routes');

const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: false}));

//HTTP
app.disable('etag');
app.use(helmet());
app.disable('x-powered-by');


//Logger
app.use(morgan('dev'));

//Router
app.use('/', router.vkNotifier);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// Error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error   = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.json({error: err});
});

module.exports = app;
