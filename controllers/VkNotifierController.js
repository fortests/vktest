const VkNotifier = require('./../modules/vkNotifier');

const currentVkNotifier = new VkNotifier();

const VkNotifierController = {
    send(req, res, next) {
        if (!req.query.template) {
            res.status(400);
            res.json({
                         error: 'Empty template',
                     });
            return;
        }

        let message = decodeURI(req.query.template).trim();

        if (currentVkNotifier.status !== VkNotifier.STATUS_ACTIVE) {
            res.status(200);
            currentVkNotifier.start(message);
        } else {
            res.status(400);
        }

        res.json({
                     status : currentVkNotifier.status,
                     batch  : currentVkNotifier.batchNumber,
                     message: currentVkNotifier.message,
                 });
    },
    getInfo(req, res, next) {
        res.status(200);

        res.json({
                     status : currentVkNotifier.status,
                     batch  : currentVkNotifier.batchNumber,
                     message: currentVkNotifier.message,
                 });
    },
    stop(req, res, next) {
        currentVkNotifier.stop();

        res.status(200);
        res.json({
                     status : currentVkNotifier.status,
                     batch  : currentVkNotifier.batchNumber,
                     message: currentVkNotifier.message,
                 });
    },
    resume(req, res, next) {
        currentVkNotifier.resume();

        res.status(200);
        res.json({
                     status : currentVkNotifier.status,
                     batch  : currentVkNotifier.batchNumber,
                     message: currentVkNotifier.message,
                 });
    },
};

module.exports = VkNotifierController;