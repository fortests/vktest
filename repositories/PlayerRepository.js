const MysqlConnection = require('./../modules/mysqlConnection');

const PlayerRepository = {
    getAllCount() {
        return MysqlConnection.query('SELECT COUNT(*) as count FROM Player').then(
            (rows) => {
                return rows[0].count;
            },
        );
    },
    getAll(limit, offset) {
        return MysqlConnection.query('SELECT * FROM Player LIMIT :offset, :limit',
                                     {offset: offset, limit: limit}).then(
            (rows) => {
                return rows;
            },
        );
    },
    getAllVkIds(limit, offset) {
        return MysqlConnection.query('SELECT vkId as id FROM Player LIMIT :offset, :limit',
                                     {offset: offset, limit: limit})
            .then(
                (rows) => {
                    return rows.map(({id}) => id);
                },
            );
    },
};

module.exports = PlayerRepository;