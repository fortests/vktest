const MysqlConnection = require('./../modules/mysqlConnection');

const VkNotifierRepository = {
    getById(id) {
        return MysqlConnection.query(
            'SELECT status as status, message as message, batchNumber as batchNumber ' +
            'FROM VkNotifier WHERE id = :id LIMIT 1',
            {id: id}).then(
            (rows) => {
                return rows[0];
            },
        );
    },
    save(id, batchNumber, message, status) {
        return MysqlConnection.query('INSERT INTO VkNotifier\n' +
                                     ' (id, batchNumber, message, status)\n' +
                                     'VALUES\n' +
                                     ' (:id, :batchNumber, :message, :status)\n' +
                                     'ON DUPLICATE KEY UPDATE\n' +
                                     '    batchNumber = :batchNumber,\n' +
                                     '    message = :message,\n' +
                                     '    status = :status\n',
                                     {
                                         id         : id,
                                         batchNumber: batchNumber,
                                         message    : message,
                                         status     : status,
                                     }).then(
            (rows) => {
                return rows;
            },
        );
    },
};

module.exports = VkNotifierRepository;