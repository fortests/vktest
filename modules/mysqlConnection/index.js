const config = require('./../../config');
const mysql  = require('mysql');

const MysqlConnection = {
    connection: null,
    getConnection() {
        if (!this.connection) {
            this.connect();

            return this.connection;
        } else {
            return this.connection;
        }
    },
    connect() {
        this.connection = mysql.createConnection({
                                                     host    : config.db.host,
                                                     user    : config.db.username,
                                                     password: config.db.password,
                                                     database: config.db.database,
                                                 });

        this.connection.config.queryFormat = function (query, values) {
            if (!values) {
                return query;
            }
            return query.replace(/\:(\w+)/g, function (txt, key) {
                if (values.hasOwnProperty(key)) {
                    return this.escape(values[key]);
                }
                return txt;
            }.bind(this));
        };

        this.connection.connect();
    },
    query(query, params) {
        return new Promise((resolve, reject) => {
            this.getConnection().query(query, params, (error, rows, fields) => {
                if (error) {
                    return reject(error);
                }

                return resolve(rows);
            });
        });
    },
};

module.exports = MysqlConnection;