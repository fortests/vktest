const config               = require('./../../config');
const WinstonLogger        = require('./../winstonLogger');
const VkApi                = require('./../vkApi');
const PlayerRepository       = require('./../../repositories/PlayerRepository');
const VkNotifierRepository = require('./../../repositories/VkNotifierRepository');
const crypto               = require('crypto');

const STATUS_STOP   = 'stop';
const STATUS_ACTIVE = 'active';
const STATUS_FINISH = 'finish';
const STATUS_ERROR  = 'error';

class VkNotifier {

    constructor() {
        let lastNotifier = VkNotifierRepository.getById(1);
        if (lastNotifier) {
            this.message     = lastNotifier.message;
            this.status      = lastNotifier.status;
            this.batchNumber = lastNotifier.batchNumber;
        } else {
            this.message     = null;
            this.status      = null;
            this.batchNumber = 0;
        }

        this.reTryBatch     = [];
        this.defaultTimeout = config.vkNotifier.defaultTimeout;
        this.currentTimeout = this.defaultTimeout;
        this.timeoutUps     = 0;
        this.maxTimeoutUps  = config.vkNotifier.maxTimeoutUps;

        this.maxCallApiCount = config.vkNotifier.maxCallApiCount;
        this.limit           = config.vkNotifier.limit;
    }

    static get STATUS_STOP() {
        return STATUS_STOP;
    }

    static get STATUS_ACTIVE() {
        return STATUS_ACTIVE;
    }

    static get STATUS_FINISH() {
        return STATUS_FINISH;
    }

    static get STATUS_ERROR() {
        return STATUS_ERROR;
    }

    get batchLimit() {
        return this.limit * this.maxCallApiCount;
    }

    get batchOffset() {
        return this.batchNumber * this.batchLimit;
    }

    start(message) {
        if (this.status === VkNotifier.STATUS_ACTIVE) {
            WinstonLogger
                .vkNotifier
                .warn(
                    `Try start VkNotifier with message: ${message}, but already active!`,
                    {
                        currentMessage    : this.message,
                        currentBatchNumber: this.batchNumber,
                    },
                );

            return true;
        }

        WinstonLogger.vkNotifier.info(`Start VkNotifier with message`,
                                      {
                                          currentMessage: message,
                                      });

        this.status         = VkNotifier.STATUS_ACTIVE;
        this.batchNumber    = 0;
        this.reTryBatch     = [];
        this.timeoutUps     = 0;
        this.currentTimeout = this.defaultTimeout;
        this.message        = message;

        return VkNotifierRepository.save(1, this.batchNumber, this.message, this.status).then(() => {
            return this.send();
        });
    };

    send() {
        WinstonLogger.vkNotifier.info(`Getting players count for notify...`);

        PlayerRepository.getAllCount()
            .then(
                (playerAllCount) => {
                    if (playerAllCount < 1) {
                        this.status = VkNotifier.STATUS_FINISH;
                        WinstonLogger.vkNotifier.warn(`Empty players count!`);
                        return true;
                    }
                    WinstonLogger.vkNotifier.info(`Get count ${playerAllCount} players`);

                    return this.runSend(playerAllCount);
                },
            )
            .then((result) => {
                return WinstonLogger.vkNotifier.error(`Stop VkNotifier`,
                                                      {
                                                          status            : this.status,
                                                          currentMessage    : this.message,
                                                          currentBatchNumber: this.batchNumber,
                                                      });
            })
            .catch((error) => {
                WinstonLogger.vkNotifier.error(`VK notification fatal error!`, {error: error});

                this.status = VkNotifier.STATUS_ERROR;
            });


    };

    stop() {
        WinstonLogger.vkNotifier.info(`Manual stop VkNotifier`);

        this.status = VkNotifier.STATUS_STOP;

        return true;
    };

    resume() {
        WinstonLogger.vkNotifier.info(`Manual resume VkNotifier`);

        this.status = VkNotifier.STATUS_ACTIVE;

        this.send();

        return true;
    };

    upBatch() {
        this.batchNumber++;

        return VkNotifierRepository.save(1, this.batchNumber, this.message, this.status);
    };

    runSend(playerAllCount) {
        if (this.status !== VkNotifier.STATUS_ACTIVE) {
            return true;
        }

        return new Promise((resolve, reject) => {
            this.getBatchAndSend()
                .then((result) => {
                    if (this.status !== VkNotifier.STATUS_ACTIVE) {
                        return resolve(true);
                    }

                    return this.upBatch();
                })
                .then(() => {
                    if (this.batchOffset < playerAllCount) {

                        setTimeout(() => {
                            return resolve(this.runSend(playerAllCount));
                        }, this.currentTimeout);

                    } else {
                        this.status = VkNotifier.STATUS_FINISH;

                        return resolve(true);
                    }
                });
        });
    };

    getBatchAndSend() {
        WinstonLogger.vkNotifier.info(`Getting players batch ...`, {
            batchNumber: this.batchNumber,
            limit      : this.batchLimit,
            offset     : this.batchOffset,
        });

        return PlayerRepository.getAllVkIds(this.batchLimit, this.batchOffset).then(
            (players) => {
                WinstonLogger.vkNotifier.info(`Players batch get`);
                return this.sendToVk(players);
            },
        );
    };

    sendToVk(playerIds) {
        return new Promise((resolve, reject) => {
            let idsBatches = [];
            let calls      = [];

            for (let i = 0; i < this.maxCallApiCount; i++) {
                calls.push(VkApi.sendNotification);

                let idsBatch = playerIds.slice(i, i + this.limit);
                idsBatches.push(idsBatch);
                if (idsBatch.length < this.limit || idsBatch.length === playerIds.length) {
                    break;
                }
            }

            WinstonLogger.vkNotifier.info(`Sending calls to VK ...`, {
                callApiCount: calls.length,
                idsBatches  : idsBatches,
            });

            Promise.all(calls.map((send, index) => send(this.message, idsBatches[index])))
                .then((results) => {
                    let success = true;
                    results.forEach((result, index) => {
                        if (result.error) {
                            WinstonLogger.vkNotifier.error(`VK notification error!`, {
                                result: result,
                            });

                            if (result.error.error_code === config.vk.errorFrequencyCode &&
                                this.timeoutUps < this.maxTimeoutUps) {

                                if (success) {
                                    this.currentTimeout += this.defaultTimeout;
                                    this.timeoutUps++;
                                }

                                this.reTryBatch = this.reTryBatch.concat(idsBatches[index]);
                            } else {
                                this.reTryBatch = [];
                                this.status     = VkNotifier.STATUS_ERROR;
                            }

                            success = false;
                        } else {
                            WinstonLogger.vkNotifier.info(`VK notification success!`, {
                                result: result,
                            });
                        }
                    });

                    if (success) {
                        this.currentTimeout = this.defaultTimeout;
                        this.timeoutUps     = 0;
                        this.reTryBatch     = [];

                        return resolve(true);
                    } else {
                        if (this.reTryBatch.length) {
                            WinstonLogger.vkNotifier.info(`ReTry batch...`, {
                                timeout   : this.currentTimeout,
                                reTryBatch: this.reTryBatch,
                            });

                            setTimeout(() => {
                                return resolve(this.sendToVk(this.reTryBatch));
                            }, this.currentTimeout);
                        } else {
                            this.status = VkNotifier.STATUS_ERROR;

                            return resolve(false);
                        }
                    }
                });
        });

    };

}

module.exports = VkNotifier;