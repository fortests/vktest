const config  = require('./../../config');
const winston = require('winston');

let winstonLogger = {
    vkNotifier: winston.createLogger({
                                         transports : [
                                             new winston.transports.Console(),
                                             new winston.transports.File(config.log.vkNotifier.file),
                                         ],
                                         exitOnError: false,
                                     }),
};

module.exports = winstonLogger;