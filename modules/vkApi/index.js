const config = require('./../../config');
const https  = require('https');

const VkApi = {
    sendNotification(message, ids) {
        const url = config.vk.sendNotificationUrl;

        let urlParams      = {
            access_token: config.vk.serviceKey,
            v           : config.vk.version,
        };
        urlParams.message  = encodeURIComponent(message);
        urlParams.user_ids = ids.join(',');

        // const getParams = helper.objectToGetParams(urlParams);

        return new Promise((resolve, reject) => {
            let request = https.get(url, urlParams, (response) => {
                // reject on bad status
                if (response.statusCode < 200 || response.statusCode >= 300) {
                    reject(new Error(`statusCode=${response.statusCode}`));
                }

                // A chunk of data has been recieved.
                let data = '';
                response.on('data', (chunk) => {
                    data += chunk;
                });

                // The whole response has been received. Print out the result.
                response.on('end', () => {
                    try {
                        let dataObj = JSON.parse(data);
                        resolve(dataObj);
                    } catch (error) {
                        reject(error);
                    }
                });

            });

            request.on('error', (error) => {
                reject(error);
            });

            request.end();
        });
    },
};

module.exports = VkApi;